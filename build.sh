#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Please specify image tag revision via script parameter"
fi
IMAGE_NAME=docker.atlassian.io/bamboo/agent-fastlane
TAG_VERSION=$1
IMAGE_TAG_NAME=$IMAGE_NAME:$TAG_VERSION

docker build -t $IMAGE_TAG_NAME .
docker push $IMAGE_TAG_NAME